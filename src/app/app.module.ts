import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioAdminComponent } from './Usuarios/inicio-admin/inicio-admin.component';
import { LoginComponent } from './Usuarios/login/login.component';
import { InicioClienteComponent } from './Usuarios/inicio-cliente/inicio-cliente.component';
import { LsClienteComponent } from './Clientes/ls-cliente/ls-cliente.component';
import { LsCreditosComponent } from './Creditos/ls-creditos/ls-creditos.component';
import { LsPagosComponent } from './Pagos/ls-pagos/ls-pagos.component';
import { LsUsuariosComponent } from './Usuarios/ls-usuarios/ls-usuarios.component';
import { FormsModule } from '@angular/forms'
import { ServicioService } from 'src/app/Servicio/servicio.service';
import { HttpClientModule } from '@angular/common/http';
import { SrusuariosService } from './Servicio/srusuarios.service';
import { SrcreditoService } from './Servicio/srcredito.service';
import { SrpagoService } from './Servicio/srpago.service';
import { AddClienteComponent } from './Clientes/add-cliente/add-cliente.component';
import { EditClienteComponent } from './Clientes/edit-cliente/edit-cliente.component';
import { AddCreditosComponent } from './Creditos/add-creditos/add-creditos.component';
import { EditCreditosComponent } from './Creditos/edit-creditos/edit-creditos.component';
import { EditPagosComponent } from './Pagos/edit-pagos/edit-pagos.component';
import { AddPagosComponent } from './Pagos/add-pagos/add-pagos.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'admin', component: InicioAdminComponent },
  { path: 'clientes', component: LsClienteComponent },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    InicioAdminComponent,
    LoginComponent,
    InicioClienteComponent,
    LsClienteComponent,
    LsCreditosComponent,
    LsPagosComponent,
    LsUsuariosComponent,
    AddClienteComponent,
    EditClienteComponent,
    AddCreditosComponent,
    EditCreditosComponent,
    EditPagosComponent,
    AddPagosComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ServicioService, SrusuariosService, SrcreditoService, SrpagoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
