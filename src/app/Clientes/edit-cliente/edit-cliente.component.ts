import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/Servicio/servicio.service';
import { Router } from '@angular/router';
import { mdCliente } from 'src/app/Modelos/mdCliente';

@Component({
  selector: 'app-edit-cliente',
  templateUrl: './edit-cliente.component.html',
  styleUrls: ['./edit-cliente.component.css']
})
export class EditClienteComponent implements OnInit {

  cliente:mdCliente;
  constructor(private router:Router, private servicio:ServicioService) { }

  ngOnInit() {
  }

  Editar() {
    let id = localStorage.getItem("id");
    this.servicio.getClienteId(+id).subscribe(data=>{
      this.cliente = data;
    })
  }
  Actualizar(clientes:mdCliente) {
    this.servicio.updateCliente(clientes).subscribe(data=>{
      this.cliente = data;
      alert("Pos se actualizó");
      // this.router.navigate("listar");
    })
  }
}
