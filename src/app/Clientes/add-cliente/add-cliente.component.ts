import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicioService } from 'src/app/Servicio/servicio.service';
import { mdCliente } from 'src/app/Modelos/mdCliente';

@Component({
  selector: 'app-add-cliente',
  templateUrl: './add-cliente.component.html',
  styleUrls: ['./add-cliente.component.css']
})
export class AddClienteComponent implements OnInit {

  constructor(private router:Router, private servicio:ServicioService) { }

  ngOnInit() {
  }

  Guardar(cliente:mdCliente) {
    this.servicio.createClientes(cliente).subscribe(data=>{
      alert("Se Agregó Exitosamente");
      // this.router.navigate("listar");
    })
  }
}
