import { Component, OnInit } from '@angular/core';
import { mdCliente } from 'src/app/Modelos/mdCliente';
import { ServicioService } from 'src/app/Servicio/servicio.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ls-cliente',
  templateUrl: './ls-cliente.component.html',
  styleUrls: ['./ls-cliente.component.css']
})
export class LsClienteComponent implements OnInit {

  mdCliente:mdCliente[];
  constructor(private servicio:ServicioService, private router:Router) { }

  ngOnInit() {
    this.servicio.getClientes().subscribe(data=>{
      this.mdCliente=data;
    })
  }
  Editar(cliente:mdCliente) {
    localStorage.setItem("id", cliente.id.toString());
    // this.router.navigate("edit");
  }
}
