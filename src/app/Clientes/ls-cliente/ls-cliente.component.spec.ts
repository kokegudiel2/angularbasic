import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LsClienteComponent } from './ls-cliente.component';

describe('LsClienteComponent', () => {
  let component: LsClienteComponent;
  let fixture: ComponentFixture<LsClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LsClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
