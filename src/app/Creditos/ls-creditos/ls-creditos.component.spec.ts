import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LsCreditosComponent } from './ls-creditos.component';

describe('LsCreditosComponent', () => {
  let component: LsCreditosComponent;
  let fixture: ComponentFixture<LsCreditosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsCreditosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LsCreditosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
