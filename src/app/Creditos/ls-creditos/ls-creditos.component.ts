import { Component, OnInit } from '@angular/core';
import { mdCredito } from 'src/app/Modelos/mdCredito';
import { SrcreditoService } from 'src/app/Servicio/srcredito.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ls-creditos',
  templateUrl: './ls-creditos.component.html',
  styleUrls: ['./ls-creditos.component.css']
})
export class LsCreditosComponent implements OnInit {

  Creditos: mdCredito[];
  constructor(private servicio: SrcreditoService, private router: Router) { }

  ngOnInit() {
    this.servicio.getCreditos().subscribe(data => {
      this.Creditos = data;
    })
  }
  Editar(credito:mdCredito) {
    localStorage.setItem("id", credito.id.toString());
    // this.router.navigate("edit");
  }
}
