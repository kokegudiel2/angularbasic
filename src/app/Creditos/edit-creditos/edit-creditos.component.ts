import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SrcreditoService } from 'src/app/Servicio/srcredito.service';
import { mdCredito } from 'src/app/Modelos/mdCredito';

@Component({
  selector: 'app-edit-creditos',
  templateUrl: './edit-creditos.component.html',
  styleUrls: ['./edit-creditos.component.css']
})
export class EditCreditosComponent implements OnInit {

  credito:mdCredito;
  constructor(private router:Router, private servicio:SrcreditoService) { }

  ngOnInit() {
    this.Editar();
  }

  Editar() {
    let id = localStorage.getItem("id");
    this.servicio.getCreditoId(+id).subscribe(data=>{
      this.credito = data;
    })
  }

  Actualizar(creditos:mdCredito) {
    this.servicio.updateCredito(creditos).subscribe(data=>{
      this.credito = data;
      alert("Pos se actualizó");
      // this.router.navigate("listar");
    })
  }
}
