import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCreditosComponent } from './edit-creditos.component';

describe('EditCreditosComponent', () => {
  let component: EditCreditosComponent;
  let fixture: ComponentFixture<EditCreditosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCreditosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCreditosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
