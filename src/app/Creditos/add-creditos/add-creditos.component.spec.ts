import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCreditosComponent } from './add-creditos.component';

describe('AddCreditosComponent', () => {
  let component: AddCreditosComponent;
  let fixture: ComponentFixture<AddCreditosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCreditosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCreditosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
