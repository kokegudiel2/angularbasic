import { Component, OnInit } from '@angular/core';
import { SrcreditoService } from 'src/app/Servicio/srcredito.service';
import { Router } from '@angular/router';
import { mdCredito } from 'src/app/Modelos/mdCredito';

@Component({
  selector: 'app-add-creditos',
  templateUrl: './add-creditos.component.html',
  styleUrls: ['./add-creditos.component.css']
})
export class AddCreditosComponent implements OnInit {

  constructor(private router:Router, private servicio:SrcreditoService) { }

  ngOnInit() {
  }

  Guardar(credito:mdCredito) {
    this.servicio.createCredito(credito).subscribe(data=>{
      alert("Se Agregó Exitosamente");
      // this.router.navigate("listar");
    })
  }
}
