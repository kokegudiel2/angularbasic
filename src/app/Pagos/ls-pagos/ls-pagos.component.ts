import { Component, OnInit } from '@angular/core';
import { mdPago } from 'src/app/Modelos/mdPago';
import { SrpagoService } from 'src/app/Servicio/srpago.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ls-pagos',
  templateUrl: './ls-pagos.component.html',
  styleUrls: ['./ls-pagos.component.css']
})
export class LsPagosComponent implements OnInit {

  Pagos:mdPago[];
  constructor(private servicio:SrpagoService, private router:Router) { }

  ngOnInit() {
    this.servicio.getPagos().subscribe(data=>{
      this.Pagos=data;
    })
  }
  Editar(pago:mdPago) {
    localStorage.setItem("id", pago.id.toString());
    // this.router.navigate("edit");
  }
}
