import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LsPagosComponent } from './ls-pagos.component';

describe('LsPagosComponent', () => {
  let component: LsPagosComponent;
  let fixture: ComponentFixture<LsPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LsPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
