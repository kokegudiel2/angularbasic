import { Component, OnInit } from '@angular/core';
import { mdPago } from 'src/app/Modelos/mdPago';
import { Router } from '@angular/router';
import { SrpagoService } from 'src/app/Servicio/srpago.service';

@Component({
  selector: 'app-edit-pagos',
  templateUrl: './edit-pagos.component.html',
  styleUrls: ['./edit-pagos.component.css']
})
export class EditPagosComponent implements OnInit {

  pago:mdPago;
  constructor(private router:Router, private servicio:SrpagoService) { }

  ngOnInit() {
  }

  Editar() {
    let id = localStorage.getItem("id");
    this.servicio.getPagoId(+id).subscribe(data=>{
      this.pago = data;
    })
  }

  Actualizar(pagos:mdPago) {
    this.servicio.updatePago(pagos).subscribe(data=>{
      this.pago = data;
      alert("Pos se actualizó");
      // this.router.navigate("listar");
    })
  }
}
