import { Component, OnInit } from '@angular/core';
import { mdPago } from 'src/app/Modelos/mdPago';
import { Router } from '@angular/router';
import { SrpagoService } from 'src/app/Servicio/srpago.service';

@Component({
  selector: 'app-add-pagos',
  templateUrl: './add-pagos.component.html',
  styleUrls: ['./add-pagos.component.css']
})
export class AddPagosComponent implements OnInit {

  constructor(private router:Router, private servicio:SrpagoService) { }

  ngOnInit() {
  }
  Guardar(pago:mdPago) {
    this.servicio.createPago(pago).subscribe(data=>{
      alert("Se Agregó Exitosamente");
      // this.router.navigate("listar");
    })
  }
}
