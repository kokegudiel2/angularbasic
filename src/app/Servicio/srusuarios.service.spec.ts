import { TestBed } from '@angular/core/testing';

import { SrusuariosService } from './srusuarios.service';

describe('SrusuariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SrusuariosService = TestBed.get(SrusuariosService);
    expect(service).toBeTruthy();
  });
});
