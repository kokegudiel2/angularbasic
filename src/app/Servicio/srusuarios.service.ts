import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mdUsuario } from '../Modelos/mdUsuario';

@Injectable({
  providedIn: 'root'
})
export class SrusuariosService {

  constructor(private http:HttpClient) { }

  Url = "/usuarios";

  mdUsuario:mdUsuario;
  // Obtener Ususrios
  getUsuarios() {
    return this.http.get<mdUsuario[]>(this.Url);
  }
}
