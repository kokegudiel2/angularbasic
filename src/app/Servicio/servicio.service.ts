import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mdCliente } from '../Modelos/mdCliente';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  constructor(private http:HttpClient) { }

  Url="/clientes";

  mdCliente:mdCliente[];
  //Obtener Clientes
  getClientes() {
    return this.http.get<mdCliente[]>(this.Url);
  }
  createClientes(cliente:mdCliente) {
    return this.http.post<mdCliente>(this.Url, cliente);
  }
  getClienteId(id: number) {
    return this.http.get<mdCliente>(this.Url + "/" + id);
  }
  updateCliente(cliente:mdCliente) {
    return this.http.put<mdCliente>(this.Url, cliente);
  }
}
