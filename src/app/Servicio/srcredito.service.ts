import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mdCredito } from '../Modelos/mdCredito';

@Injectable({
  providedIn: 'root'
})
export class SrcreditoService {

  constructor(private http: HttpClient) { }

  Url = "/creditos";

  md_credito: mdCredito[];
  // Obtener Credito
  getCreditos() {
    return this.http.get<mdCredito[]>(this.Url);
  }
  createCredito(credito: mdCredito) {
    return this.http.post<mdCredito>(this.Url, credito);
  }
  getCreditoId(id: number) {
    return this.http.get<mdCredito>(this.Url + "/" + id);
  }
  updateCredito(credito: mdCredito) {
    return this.http.put<mdCredito>(this.Url, credito);
  }
}
