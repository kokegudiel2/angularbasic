import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { mdPago } from '../Modelos/mdPago';

@Injectable({
  providedIn: 'root'
})
export class SrpagoService {

  constructor(private http:HttpClient) { }

  Url = "/pagos";

  mdPago:mdPago[];
  // Obtener pagos
  getPagos() {
    return this.http.get<mdPago[]>(this.Url);
  }
  createPago(pago:mdPago) {
    return this.http.post<mdPago>(this.Url, pago);
  }
  getPagoId(id: number) {
    return this.http.get<mdPago>(this.Url + "/" + id);
  }
  updatePago(pago: mdPago) {
    return this.http.put<mdPago>(this.Url, pago);
  }
}
