import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LsUsuariosComponent } from './ls-usuarios.component';

describe('LsUsuariosComponent', () => {
  let component: LsUsuariosComponent;
  let fixture: ComponentFixture<LsUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LsUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
