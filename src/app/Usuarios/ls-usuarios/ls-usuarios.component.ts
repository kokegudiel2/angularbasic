import { Component, OnInit } from '@angular/core';
import { mdUsuario } from 'src/app/Modelos/mdUsuario';
import { SrusuariosService } from 'src/app/Servicio/srusuarios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ls-usuarios',
  templateUrl: './ls-usuarios.component.html',
  styleUrls: ['./ls-usuarios.component.css']
})
export class LsUsuariosComponent implements OnInit {

  Usuarios:mdUsuario[];
  constructor(private servicio:SrusuariosService, private router:Router) { }

  ngOnInit() {
    this.servicio.getUsuarios().subscribe(data=>{
      this.Usuarios=data;
    })
  }

}
