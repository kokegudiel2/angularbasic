import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AddClienteComponent } from './Clientes/add-cliente/add-cliente.component';
// import { EditClienteComponent } from './Clientes/edit-cliente/edit-cliente.component';
// import { AddCreditosComponent } from './Creditos/add-creditos/add-creditos.component';
// import { EditCreditosComponent } from './Creditos/edit-creditos/edit-creditos.component';
// import { AddPagosComponent } from './Pagos/add-pagos/add-pagos.component';
// import { EditPagosComponent } from './Pagos/edit-pagos/edit-pagos.component';


const routes: Routes = [
  // {path:"addCliente", component:AddClienteComponent},
  // {path:"editCliente", component:EditClienteComponent},

  // {path:"addCreditos", component:AddCreditosComponent},
  // {path:"editCreditos", component:EditCreditosComponent},

  // {path:"addPago", component:AddPagosComponent},
  // {path:"editPago", component:EditPagosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
