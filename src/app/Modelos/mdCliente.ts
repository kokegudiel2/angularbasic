export class mdCliente{
    id:Number;
    nombre:String;
    apellido:String;
    nit:String;
    dpi:String;
    telefono:String;
    direccion:String;
}