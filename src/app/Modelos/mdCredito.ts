export class mdCredito {
    id: number;
    monto: number;
    noPagos: number;
    rangoPagos: number;
    estado: string;
    fecha: Date;
    multa: number;
    clienteId: number;
    interesId: number;
}