export class mdPago {
    id: number;
    fechaPago: Date;
    monto: number;
    montoTotal: number;
    noPago: number;
    creditoId: number;
}